# PSLeap

A PowerShell smart filesystem bookmarking tool.

## Description
This module for PowerShell makes it easy to quickly jump to set bookmarks on your filesystem. Each bookmark can be set with an Include regex filter to make it context dependent. When searching for bookmarks to jump to, PSLeap will display extra information such as the path or notes that you've set about this specific location. This module also contains a modern management interface for power users.

## Installation
~~~
Install-Module -Scope CurrentUser PSLeap
~~~

## Usage
Add some leap points (called vistas)
~~~
Add-LeapLocation -Name github -Path ~\Documents\Gitlab -Notes "Contains my personal gitlab account repos"
cd C:\Windows\System32; Add-LeapLocation -Name sys32
~~~
Add a nice alias to your $PROFILE
~~~
Set-Alias -Name de -Value Set-LeapLocation # Its like cd, but de!
~~~
Go to a vista via full name match
~~~
de sys32
~~~
Go to a vista via starts-with match
~~~
cd C:\
de sys
~~~

## Demo
See it in action!

![Demo video](img/Demo.gif)

## Roadmap
No updates planned.

## Contributing
Feel free to submit an issue or MR!

## License
MIT

## Project status
This was a ~4 hour module. It was just to see if I could make a decent argument completer implementation.
