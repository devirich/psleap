﻿function Get-LeapInternal {
    <#
    .SYNOPSIS
    Retrieves the module internal object for fine tuning and manipulation

    .DESCRIPTION
    The object returned is used to set various settings for storing, retrieving, and displaying vistas.


    .EXAMPLE
    PS> $internal = Get-Leapinternal

    .EXAMPLE
    PS> $internal.ListFormat = "{0}"

    Change the name of the text in the list view (PSReadline Ctrl + Space) for Set-LeapLocation
    to only display the name of the vista.
    (Default is to display the name and if it is a temporary/in-memory vista)
    See https://gitlab.com/devirich/psleap/-/blob/main/src/Private/~import.ps1#L14-19 for possible replacements

    .EXAMPLE
    PS> $internal.TemporaryDesignation = " (!!)"

    Change the text to be used to denote which vistas are temporary (in memory only) in Set-LeapLocation parameter list view

    .EXAMPLE
    PS> $internal.PersistentArgumentList[0].Notes = "Cooler notes"
    PS> $internal.Export()

    Update the vista persistent database and set the 1st item's Notes to "Cooler Notes"

    .EXAMPLE
    PS> $internal.Update()

    Update the list view database (such as after changing data in the vista persistent database or an entry in $internal.TemporaryArgumentList)

    .EXAMPLE
    PS> $internal.DefaultStore = "Temporary"

    Set the default location for new vistas to be in memory instead of stored persistently to disk.

    .EXAMPLE
    PS> $internal.StartsWithFallback = $false

    Enable strict matching of vista names (default is true and will attempt to find a matching vista that starts with your $name text)

    .EXAMPLE
    #>
    [cmdletbinding()]
    param()
    $script:orb
}
