function Set-LeapLocation {
    <#
    .SYNOPSIS
    Changes your current directory to a leap point (as stored in either the vista file or in memory)

    .PARAMETER Name
    The name of the leap point (a vista!) that you want to travel to.

    .EXAMPLE
    Jumps to the specified leap point/vista (assuming you have a leap point titled sys32)
    PS> Set-LeapPoint sys32

    .EXAMPLE
    Jumps to the specified leap point/vista
    Assuming you have a leap point titled sys32 and StartsWithFallback has been left on
    PS> Set-LeapPoint sys

    .NOTES
    General notes
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]$Name
    )

    if ($bookmark = $script:orb.Get($Name)) {
        Set-Location $bookmark.Path
    }
    else {
        throw "No leap point found for query '$Name'. Exiting."
    }
}
