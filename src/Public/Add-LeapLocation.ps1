function Add-LeapLocation {
    <#
    .SYNOPSIS
    Adds a leap point (A vista!) to the leap points database

    .DESCRIPTION
    Long description

    .PARAMETER Name
    The name of the leap point to save

    .PARAMETER Path
    The path to save (can be relative or absolute)

    .PARAMETER Notes
    Any notes that you want to save about the vista

    .PARAMETER Include
    A regex pattern that will be checked against each vista path when retrieving vista

    .PARAMETER Scope
    Whether to store the leap point in memory only or persistently in the vista database

    .EXAMPLE
    Adds a persistent leap point with the name, path, and notes specified.
    PS> Add-LeapLocation -Name github -Path ~\Documents\Gitlab -Notes "Contains my personal gitlab account repos"

    .EXAMPLE
    Adds a persistent leap point to the current path with the specified name.
    PS> cd C:\Windows\System32; Add-LeapLocation -Name sys32

    #>
    [CmdletBinding()]
    param (
        $Name,
        $Path = $pwd,
        $Notes,
        $Include,
        [ValidateSet("Persistent", "Temporary")]
        $Scope = $script:orb.DefaultStore
    )

    $item = [Bookmark]@{
        Name    = $Name
        Path    = $Path
        Notes   = $Notes
        Include = $Include
    }
    if ($Scope -eq "Temporary") {
        $item.TempText = $script:orb.TemporaryDesignation
        $script:orb.TempArgumentList += $item
        $script:orb.Update()
    }
    else {
        $script:orb.PersistentArgumentList += $item
        $script:orb.Export()
    }
}
