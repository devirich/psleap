# I'm calling the master object an orb because it is short and sweet.
$script:orb = [LeapData]::new()

$script = {
    param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameter )

    $script:orb.ArgumentList |
    Where-Object name -Like "$wordToComplete*" |
    Where-Object { $pwd -match $_.Include } |
    ForEach-Object {
        if ($_.Include) { $include = "`nInclude filter: {2}" }
        else { $include = '' }
        [System.Management.Automation.CompletionResult]::new(
            $_.Name,
            ($script:orb.ListFormat -f @(
                $_.Name
                $_.TempText
                $_.Path
                $_.Notes
                $_.Include
                Split-Path -Leaf $_.Path
            )),
            "ParameterValue",
            ("{0}$include`n{1}" -f $_.Path, $_.Notes, $_.Include)
        )
    }

}
Register-ArgumentCompleter -CommandName Set-LeapLocation -ScriptBlock $script -ParameterName Name

$script:orb.Update()
