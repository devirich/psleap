class Bookmark {
    [parameter(Mandatory)]$Name
    [string]$Path
    [string]$Notes
    [string]$Include
    [string]$TempText

    Bookmark() {}
    Bookmark([object]$obj) {
        $this.Name = $obj.Name
        $this.Path = $obj.Path
        $this.Notes = $obj.Notes
        $this.Include = $obj.Include
        $this.TempText = $obj.TempText
    }
    Bookmark([string]$str) {
        $this.Name = $str
    }
}

class LeapData {
    $DatabaseName = "PSLeapVistas.csv"
    $DatabaseDirectory = "~\Documents\PowerShell\Config\PSLeap"
    [ValidateSet("Persistent", "Temporary")]
    $DefaultStore = "Persistent"
    [string]$TemporaryDesignation = " (Temp)"
    [string]$ListFormat = "{0}{1}"
    $StartsWithFallback = $true
    [bookmark[]]$PersistentArgumentList
    [bookmark[]]$TempArgumentList
    hidden [bookmark[]]$ArgumentList

    LeapData() {}

    [bookmark[]] Get() { return $this.ArgumentList }
    [bookmark] Get([string]$Name) {
        if ($out = $this.ArgumentList.Where{ $_.Name -eq $Name }[0]) {}
        elseif ($this.StartsWithFallback -and ($out = $this.ArgumentList.Where{ $_.Name -like "$Name*" }[0])) {}

        if ($out) { return $out }
        else { return $null }
    }

    Update() {
        $this.ArgumentList = @()
        if ($this.TempArgumentList.count) {
            $this.ArgumentList += $this.TempArgumentList | Sort-Object Name
        }

        # Get persistent locations and add them to the db content
        if (Test-Path "$($script:orb.DatabaseDirectory)\$($script:orb.DatabaseName)") {
            $this.PersistentArgumentList = Import-Csv "$($script:orb.DatabaseDirectory)\$($script:orb.DatabaseName)"
            $this.ArgumentList += $this.PersistentArgumentList | Sort-Object Name
        }
    }

    Export() {
        mkdir -ea silent $this.DatabaseDirectory | Out-Null
        $this.PersistentArgumentList | Where-Object { -not $_.TempText } | Export-Csv "$($script:orb.DatabaseDirectory)\$($script:orb.DatabaseName)"
        $this.Update()
    }
}
